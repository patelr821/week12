resource "aws_cloudwatch_log_group" "code_build_log_group" {
    name                = "code_build_log_group"
    retention_in_days   = 1
}